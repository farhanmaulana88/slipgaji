 <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url();?>index.php/login/logout">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>assets/admin/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/admin/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/admin/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo base_url();?>assets/admin/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo base_url();?>assets/admin/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/demo/chart-pie-demo.js"></script>

  <script src="<?php echo base_url().'assets/js/jquery-2.2.4.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/moment.js'?>"></script>

  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
    $('#mydata').DataTable( {
        dom: 'Bfrtip',
        buttons: [ {extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
                  {extend: 'excel',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
                {extend: 'csv',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
            'copy','print',

        ]
    } );
} );

</script>

<script>
    $(document).ready(function() {
    $('#mydata2').DataTable( {
        pageLength: 30,
        dom: 'Bfrtip',
        buttons: [ {extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
                  {extend: 'excel',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
                {extend: 'csv',
                orientation: 'landscape',
                pageSize: 'LEGAL'},
            'copy','print',

        ]
    } );
} );

</script>

 <script type="text/javascript">
        function update() {
            var msg= confirm("Apakah anda yakin ingin mengubah data ini?");
            if (msg){
              return true ;
            }else{
              return false ;
            }
        }

        function validasiDelete() {
            var msg= confirm("Apakah anda yakin ingin menghapus data ini?");
            if (msg){
              return true ;
            }else{
              return false ;
            }
        }

        function validasiAdd() {
            var msg= confirm("Apakah anda yakin ingin menambahkan data ini?");
            if (msg){
              return true ;
            }else{
              return false ;
            }
        }
    </script>

    <script>
  $(document).ready(function(){
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
  });
  </script>

  <script language="JavaScript">
  function selectAll(source) {
    var checkboxes = document.getElementsByName('check_list[]');
    for(var i in checkboxes)
      checkboxes[i].checked = source.checked;
  }
</script>

</body>

</html>
