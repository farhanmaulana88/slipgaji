 <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Tabel / Tabel Pegawai</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Pegawai (Gaji Pokok)</h6>
            </div>
            <div class="card-body">
                <div class="my-2"></div>
                <button data-toggle="modal" data-target="#modal_add" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-pen"></i>
                    </span>
                    <span class="text">Add</span>
                  </button>
                  <a href="<?php echo base_url();?>index.php/admin/deletePegawai" onclick="return update();" class="btn btn-danger btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Delete All</span>
                  </a>
              <div class="table-responsive">
                <br>
                <table class="table table-bordered" id="mydata" width="100%" cellspacing="0">
                 	<thead>
	 <tr>
    <th>No.</th>
    <th>NIP</th>
    <th>Nama Pegawai</th>
    <th>Golongan</th>
    <th>Rekening</th>
    <th>Nama Rekening</th>
    <th>Nama Bank</th>
    <th>Email</th>
    <th></th>
  </tr>
		</thead>
		<tbody>
			    <?php $no=0;
        foreach($pegawai->result_array() as $i):
            $nip=$i['nip'];
            $nama_pegawai=$i['nama_pegawai'];
            $golongan=$i['golongan'];
            $rekening=$i['rekening'];
            $nama_rekening=$i['nama_rekening'];
            $nama_bank=$i['nama_bank'];
            $email=$i['email'];
            $no++;
        ?>
			<td><?= $no;?></td>
      <td><a href="<?= base_url('index.php/admin/getDetailPegawai/'.$nip);?>" title="Detail"><?= $nip;?></td>
      <td><?= $nama_pegawai;?></td>
      <td><?= $golongan;?></td>
      <td><?= $rekening;?></td>
      <td><?= $nama_rekening;?></td>
      <td><?= $nama_bank;?></td>
      <td><?= $email;?></td>
      <td>
        <button class="btn btn-xs btn-info" style="width: 72px" data-toggle="modal" data-target="#modal_edit<?= $nip;?>">Edit</button>
      	<a href="<?php echo base_url('index.php/admin/deleteDetailPegawai/'.$nip);?>" onclick="return validasiDelete();" class="btn btn-xs btn-danger">Delete</a>
      </td>
      </tr>
			<?php endforeach;?>
		</tbody>
                 
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

                       <!-- ============ MODAL EDIT BARANG =============== -->
    <?php 
        foreach($pegawai->result_array() as $i):
            $nip=$i['nip'];
            $nama_pegawai=$i['nama_pegawai'];
            $golongan=$i['golongan'];
            $rekening=$i['rekening'];
            $nama_rekening=$i['nama_rekening'];
            $nama_bank=$i['nama_bank'];
            $email=$i['email'];
        ?>
 <div class="modal fade" id="modal_edit<?php echo $nip;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Edit Pegawai</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/admin/editPegawai'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIP</label>
                        <div class="col-xs-8">
                            <input name="nip" value="<?php echo $nip;?>" class="form-control" type="text" placeholder="NIP Pegawai..." readonly>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Pegawai</label>
                        <div class="col-xs-8">
                            <input name="nama_pegawai" value="<?php echo $nama_pegawai;?>" class="form-control" type="text" placeholder="Nama Pegawai...">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Golongan</label>
                        <div class="col-xs-8">
                            <input name="golongan" value="<?php echo $golongan;?>" class="form-control" type="text" placeholder="Golongan..." required>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Rekening</label>
                        <div class="col-xs-8">
                            <input name="rekening" value="<?php echo $rekening;?>" class="form-control" type="text" placeholder="Rekening..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Rekening</label>
                        <div class="col-xs-8">
                            <input name="nama_rekening" value="<?php echo $nama_rekening;?>" class="form-control" type="text" placeholder="Nama Rekening..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Bank</label>
                        <div class="col-xs-8">
                            <input name="nama_bank" value="<?php echo $nama_bank;?>" class="form-control" type="text" placeholder="Nama Bank..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Email</label>
                        <div class="col-xs-8">
                            <input name="email" value="<?php echo $email;?>" class="form-control" type="text" placeholder="Email..." required>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" onclick="return update();">Update</button>
                </div>
            </form>
            </div>
            </div>
        </div>

    <?php endforeach;?>

                           <!-- ============ MODAL ADD BARANG =============== -->

 <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Tambah Pegawai</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'index.php/admin/addPegawai'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >NIP</label>
                        <div class="col-xs-8">
                            <input name="nip" class="form-control" type="text" placeholder="NIP Pegawai..." >
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Pegawai</label>
                        <div class="col-xs-8">
                            <input name="nama_pegawai" class="form-control" type="text" placeholder="Nama Pegawai...">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Golongan</label>
                        <div class="col-xs-8">
                            <input name="golongan" class="form-control" type="text" placeholder="Golongan..." required>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Rekening</label>
                        <div class="col-xs-8">
                            <input name="rekening" class="form-control" type="text" placeholder="Rekening..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Rekening</label>
                        <div class="col-xs-8">
                            <input name="nama_rekening" class="form-control" type="text" placeholder="Nama Rekening..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Bank</label>
                        <div class="col-xs-8">
                            <input name="nama_bank" class="form-control" type="text" placeholder="Nama Bank..." required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Email</label>
                        <div class="col-xs-8">
                            <input name="email" class="form-control" type="text" placeholder="Email..." required>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" onclick="return validasiAdd();">Tambah</button>
                </div>
            </form>
            </div>
            </div>
        </div>

      </div>
      <!-- End of Main Content -->

