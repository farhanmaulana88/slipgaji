<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Tabel / Tabel Potongan</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Potongan Gaji</h6>
            </div>
            <div class="card-body">
            <form method="POST" action="<?php echo base_url();?>index.php/admin/deletePotongan">
              <div class="form-group">
                      <input name="bulan_tahun" style="width: 225px;" type="text" placeholder="Delete Berdasar Bulan Tahun.">
              <button type="submit" class="btn btn-danger btn-circle" onclick="return update();"><i class="fas fa-trash"></i></button>
                    </div>
            </form>
            <div class="my-2"></div>
                  <a href="<?php echo base_url();?>index.php/admin/deleteAllPotongan" onclick="return update();" class="btn btn-danger btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Delete All</span>
                  </a>
              <div class="table-responsive">
                <br>

                <table class="table table-bordered" id="mydata" width="100%" cellspacing="0">
        <thead>
<tr>

    <th>No.</th>
    <th>Nama Pegawai</th>
    <th>Bulan Tahun</th>
    <th>Uang Makan</th>
    <th>Tunker</th>
    <th>Simpanan Pokok</th>
    <th>Simpanan Wajib</th>
    <th>Pinjaman</th>
    <th>Bon Koperasi</th>
    <th>Warung Koperasi</th>
    <th>Arisan</th>
    <th>Konsumsi</th>
    <th>Kas D.W</th>
    <th>Dana Sosial</th>
    <th>BRI Pajajaran</th>
    <th>Pengurang Tunker</th>
    <th>Total Potongan</th>
    <th>Bayar</th>
  </tr>
        </thead>
        <tbody>
        <?php
    $no=0;
    if( ! empty($potongan)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
        foreach($potongan as $data){ // Lakukan looping pada variabel admin dari controllernip
            $no++;
            ?>
         <tr>
      <td><?= $no;?></td>
      <td><?= $data->nama_pegawai;?></td>
      <td><a class="btn btn-xs btn-info" href="<?php echo base_url('index.php/admin/getDataBulan/'.$data->bulan_tahun);?>"><?= $data->bulan_tahun;?></a></td>
      <td><?= number_format($data->uang_makan,0,',','.');?></td>
      <td><?= number_format($data->tunker,0,',','.');?></td>
      <td><?= number_format($data->p1,0,',','.');?></td>
      <td><?= number_format($data->p2,0,',','.');?></td>
      <td><?= number_format($data->p3,0,',','.');?></td>
      <td><?= number_format($data->p4,0,',','.');?></td>
      <td><?= number_format($data->p5,0,',','.');?></td>
      <td><?= number_format($data->p6,0,',','.');?></td>
      <td><?= number_format($data->p7,0,',','.');?></td>
      <td><?= number_format($data->p8,0,',','.');?></td>
      <td><?= number_format($data->p9,0,',','.');?></td>
      <td><?= number_format($data->p10,0,',','.');?></td>
      <td><?= number_format($data->pengurang_tunker,0,',','.');?></td>
      <td><?= number_format($data->total_potongan,0,',','.');?></td>
      <td><?= number_format($data->bayar,0,',','.');?></td>
      </tr>

    <?php } ?>
    <?php }else{ // Jika data tidak ada ?>
        <tr><td colspan='20'><center>No data available in table</center></td></tr>
    <?php }
    ?>
        </tbody>
    </table>
   
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

