<style type="text/css">
  div.scrollmenu {
  background-color: #333;
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu a {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}

div.scrollmenu a:hover {
  background-color: #777;
}
</style>

<body>
	<h3>Import Potongan Gaji</h3>
	<hr>

	<a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
	<br>
	<br>

	<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
	<form method="post" action="<?php echo base_url("index.php/admin/formpotongan"); ?>" enctype="multipart/form-data">
		<!--
		-- Buat sebuah input type file
		-- class pull-left berfungsi agar file input berada di sebelah kiri
		-->
		<input type="file" class="btn btn-info btn-icon-split" name="file">

		<!--
		-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
		-->
		<input type="submit" class="btn btn-success btn-icon-split" name="preview" value="Preview">
	</form>

	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("index.php/admin/import2")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";
    echo "<div class='scrollmenu'>";
		echo "<table id='mydata' border='1' cellpadding='8'>
		<tr>
			<th colspan='20'>Preview Data</th>
		</tr>
		<tr>
      <th>No</th>
      <th>Nama</th>
      <th>Bulan</th>
      <th>Uang Makan</th>
      <th>Tunker</th>
      <th>p1</th>
      <th>p2</th>
      <th>p3</th>
      <th>p4</th>
      <th>p5</th>
      <th>p6</th>
      <th>p7</th>
      <th>p8</th>
      <th>p9</th>
      <th>p10</th>
      <th>Pengurang Tunker</th>
      <th>Total Potongan</th>
      <th>Bayar</th>
    </tr>";

		$numrow = 1;
		$kosong = 0;
		$no = -1;

		// Lakukan perulangan dari data yang ada di excel
		// $sheet adalah variabel yang dikirim dari controller
		foreach($sheet as $row){
		$no++;
			// Ambil data pada excel sesuai Kolom
			$Nama = $row['D'];  
      $bulan_tahun = $row['AF'];  
      $uang_makan = $row['J'];    
      $tunker = $row['K'];
      $p1 = $row['L'];
      $p2 = $row['M'];
      $p3 = $row['N'];
      $p4 = $row['O'];
      $p5 = $row['P'];
      $p6 = $row['Q'];
      $p7 = $row['R'];
      $p8 = $row['S'];
      $p9 = $row['T'];
      $p10 = $row['U'];
      $pengurang_tunker = $row['V'];
      $total_potongan = $row['AB'];
      $bayar = $row['AD'];

			// Cek jika semua data tidak diisi
			 if($Nama == "" && $bulan_tahun == "" && $uang_makan == "" && $tunker == "" && $p1 == "" && $p2 == "" && $p3 == "" && $p4 == "" && $p5 == "" && $p6 == "" && $p7 == "" && $p8 == "" && $p9 == "" && $p10 == "" && $pengurang_tunker == "" && $total_potongan == "" && $bayar == "")
        continue; 
 
      if($numrow > 1){
       
        $nama_td = ( ! empty($Nama))? "" : " style='background: #E07171;'";  
        $bulan_tahun_td = ( ! empty($bulan_tahun))? "" : " style='background: #E07171;'"; 
        $uang_makan_td = ( ! empty($uang_makan))? "" : " style='background: #E07171;'";      
        $tunker_td = ( ! empty($tunker))? "" : " style='background: #E07171;'";       
        $p1_td = ( ! empty($p1))? "" : " style='background: #E07171;'";   
        $p2_td = ( ! empty($p2))? "" : " style='background: #E07171;'"; 
        $p3_td = ( ! empty($p3))? "" : " style='background: #E07171;'"; 
        $p4_td = ( ! empty($p4))? "" : " style='background: #E07171;'"; 
        $p5_td = ( ! empty($p5))? "" : " style='background: #E07171;'"; 
        $p6_td = ( ! empty($p6))? "" : " style='background: #E07171;'"; 
        $p7_td = ( ! empty($p7))? "" : " style='background: #E07171;'"; 
        $p8_td = ( ! empty($p8))? "" : " style='background: #E07171;'"; 
        $p9_td = ( ! empty($p9))? "" : " style='background: #E07171;'"; 
        $p10_td = ( ! empty($p10))? "" : " style='background: #E07171;'"; 
        $pengurang_tunker_td = ( ! empty($pengurang_tunker))? "" : " style='background: #E07171;'"; 
        $total_potongan_td = ( ! empty($total_potongan))? "" : " style='background: #E07171;'"; 
        $bayar_td = ( ! empty($bayar))? "" : " style='background: #E07171;'"; 
				// Jika salah satu data ada yang kosong
		  if($Nama == "" or $bulan_tahun == "" or $uang_makan == "" or $tunker == "" or $p1 == "" or $p2 == "" or $p3 == "" or $p4 == "" or $p5 == "" or $p6 == "" or $p7 == "" or $p8 == "" or $p9 == "" or $pengurang_tunker_td == "" or $total_potongan == "" or $bayar == ""){
          $kosong++; 

        }
        
        echo "<tr>";
        echo "<td>".$no."</td>";
        echo "<td".$nama_td.">".$Nama."</td>";
        echo "<td".$bulan_tahun_td.">".$bulan_tahun."</td>";
        echo "<td".$uang_makan_td.">".$uang_makan."</td>";
        echo "<td".$tunker_td.">".$tunker."</td>";
        echo "<td".$p1_td.">".$p1."</td>";
        echo "<td".$p2_td.">".$p2."</td>";
        echo "<td".$p3_td.">".$p3."</td>";
        echo "<td".$p4_td.">".$p4."</td>";
        echo "<td".$p5_td.">".$p5."</td>";
        echo "<td".$p6_td.">".$p6."</td>";
        echo "<td".$p7_td.">".$p7."</td>";
        echo "<td".$p8_td.">".$p8."</td>";
        echo "<td".$p9_td.">".$p9."</td>";
        echo "<td".$p10_td.">".$p10."</td>";
        echo "<td".$pengurang_tunker_td.">".$pengurang_tunker."</td>";
        echo "<td".$total_potongan_td.">".$total_potongan."</td>";
        echo "<td".$bayar_td.">".$bayar."</td>";
        echo "</tr>";
      }

			$numrow++; // Tambah 1 setiap kali looping
		}

    echo "</table>";
		echo "</div>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
		 // Jika semua data sudah diisi
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button class='btn btn-info btn-icon-split' type='submit' name='import'>Import</button>"; echo "&emsp;";
			echo "<a href='".base_url("index.php/admin")."'>Cancel</a>";
	

		echo "</form>";
	}
	?>