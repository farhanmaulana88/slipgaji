<style type="text/css">
  div.scrollmenu {
  background-color: #333;
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu a {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}

div.scrollmenu a:hover {
  background-color: #777;
}
</style>
<body>
	<h3>Import Data Pegawai</h3>
	<hr>

	<a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
	<br>
	<br>

	<!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
	<form method="post" action="<?php echo base_url("index.php/admin/formPegawai"); ?>" enctype="multipart/form-data">
		<!--
		-- Buat sebuah input type file
		-- class pull-left berfungsi agar file input berada di sebelah kiri
		-->
		<input class="btn btn-info btn-icon-split" type="file" name="file">

		<!--
		-- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
		-->
		<input type="submit" class="btn btn-success btn-icon-split" name="preview" value="Preview">
	</form>

	<?php
	if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
		if(isset($upload_error)){ // Jika proses upload gagal
			echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
			die; // stop skrip
		}

		// Buat sebuah tag form untuk proses import data ke database
		echo "<form method='post' action='".base_url("index.php/admin/import")."'>";

		// Buat sebuah div untuk alert validasi kosong
		echo "<div style='color: red;' id='kosong'>
		Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
		</div>";

		echo "<div class='scrollmenu'>";
		echo "<table id='mydata' border='1' cellpadding='8'>
		<tr>
			<th colspan='20'>Preview Data</th>
		</tr>
		<tr>
      <th>No.</th>
      <th>NIP</th>
      <th>Nama Pegawai</th>
      <th>Golongan</th>
      <th>Rekening</th>
      <th>Nama Rekening</th>
      <th>Nama Bank</th>
      <th>Email</th>
    </tr>";

		$numrow = 1;
		$kosong = 0;
		$no = -1;

		foreach($sheet as $row){
		$no++;
		  $nip = $row['C'];    
	      $nama_pegawai = $row['D']; 	  
	      $golongan = $row['E']; 	    
	      $rekening = $row['F']; 	      
	      $nama_rekening = $row['G']; 	      
	      $nama_bank = $row['H']; 
	      $email = $row['AE']; 

			// Cek jika semua data tidak diisi
			 if($nip == "" && $nama_pegawai == "" && $golongan == "" && $rekening == "" && $nama_rekening == "" && $nama_bank == "" && $email == "")
        	continue; 
 
      if($numrow > 1){
       
        $id_td = ( ! empty($id))? "" : " style='background: #E07171;'"; 
        $nip_td = ( ! empty($nip))? "" : " style='background: #E07171;'";    
        $nama_pegawai_td = ( ! empty($nama_pegawai))? "" : " style='background: #E07171;'";  
        $golongan_td = ( ! empty($golongan))? "" : " style='background: #E07171;'";      
        $rekening_td = ( ! empty($rekening))? "" : " style='background: #E07171;'";       
        $nama_rekening_td = ( ! empty($nama_rekening))? "" : " style='background: #E07171;'";      
        $nama_bank_td = ( ! empty($nama_bank))? "" : " style='background: #E07171;'"; 
        $email_td = ( ! empty($email))? "" : " style='background: #E07171;'"; 
				// Jika salah satu data ada yang kosong
		  if($nip == "" or $nama_pegawai == "" or $golongan == "" or $rekening == "" or $nama_rekening == "" or $nama_bank == "" or $email == ""){
          $kosong++; 

        }
        
        echo "<tr>";
        // echo "<td>".$no."</td>";
        echo "<td>".$no."</td>";
        echo "<td".$nip_td.">".$nip."</td>";
        echo "<td".$nama_pegawai_td.">".$nama_pegawai."</td>";
        echo "<td".$golongan_td.">".$golongan."</td>";
        echo "<td".$rekening_td.">".$rekening."</td>";
        echo "<td".$nama_rekening_td.">".$nama_rekening."</td>";
        echo "<td".$nama_bank_td.">".$nama_bank."</td>";
        echo "<td".$email_td.">".$email."</td>";
  
        echo "</tr>";
      }

			$numrow++; // Tambah 1 setiap kali looping
		}

		echo "</table>";
		echo "</div>";

		// Cek apakah variabel kosong lebih dari 0
		// Jika lebih dari 0, berarti ada data yang masih kosong
			echo "<hr>";

			// Buat sebuah tombol untuk mengimport data ke database
			echo "<button class='btn btn-info btn-icon-split' type='submit' name='import'>Import</button>";
			echo "<a href='".base_url("index.php/admin")."'>Cancel</a>";

		echo "</form>";
	}
	?>