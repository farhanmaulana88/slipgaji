<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Detail Potongan Gaji Pegawai</h1>
          <?php foreach ($pegawai2 as $pegawai2) { ?>
          <h5>Nama : <?= $pegawai2->nama_pegawai;?></h5>
          <p class="mb-4">NIP : <?= $pegawai2->nip;?></p>
        <?php } ?>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Data Potongan Gaji</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                  <form action="<?php echo base_url();?>index.php/admin/send_email_all" method="post">
                <table class="table table-bordered" id="mydata2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <th><input type="checkbox" id="selectall" onClick="selectAll(this)" />Select All</th>
                          <th>No.</th>
                          <th>Bulan Tahun</th>
                          <th>Uang Makan</th>
                          <th>Tunker</th>
                          <th>Simpanan Pokok</th>
                          <th>Simpanan Wajib</th>
                          <th>Pinjaman</th>
                          <th>Bon Koperasi</th>
                          <th>Warung Koperasi</th>
                          <th>Arisan</th>
                          <th>Konsumsi</th>
                          <th>Kas D.W</th>
                          <th>Dana Sosial</th>
                          <th>BRI Pajajaran</th>
                          <th>Pengurang Tunker</th>
                          <th>Total Potongan</th>
                          <th>Bayar</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                      $no=0;
                      if( ! empty($pegawai)){
                          foreach($pegawai as $data){
                          $no++;
                    ?>
                         <tr>
                          <td><input type="checkbox" name="check_list[]" value="<?= $data->id123;?>"></td>
                          <td><?= $no;?></td>
                          <td><a class="btn btn-xs btn-info" href="<?php echo base_url('index.php/admin/getDataBulan/'.$data->bulan_tahun);?>"><?= $data->bulan_tahun;?></a></td>
                          <td><?= number_format($data->uang_makan,0,',','.');?></td>
                          <td><?= number_format($data->tunker,0,',','.');?></td>
                          <td><?= number_format($data->p1,0,',','.');?></td>
                          <td><?= number_format($data->p2,0,',','.');?></td>
                          <td><?= number_format($data->p3,0,',','.');?></td>
                          <td><?= number_format($data->p4,0,',','.');?></td>
                          <td><?= number_format($data->p5,0,',','.');?></td>
                          <td><?= number_format($data->p6,0,',','.');?></td>
                          <td><?= number_format($data->p7,0,',','.');?></td>
                          <td><?= number_format($data->p8,0,',','.');?></td>
                          <td><?= number_format($data->p9,0,',','.');?></td>
                          <td><?= number_format($data->p10,0,',','.');?></td>
                          <td><?= number_format($data->pengurang_tunker,0,',','.');?></td>
                          <td><?= number_format($data->total_potongan,0,',','.');?></td>
                          <td><?= number_format($data->bayar,0,',','.');?></td>
                      </tr>
                          <?php } ?>
                    <?php } ?>
                    </tbody>
              </table>
                  <button class="btn btn-xs btn-danger" type="submit">Kirim</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

