<?php

class Admin extends CI_Controller {
	private $filename = "import_data";
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('ModelAdmin');

		if($this->session->userdata('status') != "LOGIN" 
			and $this->session->userdata('status') != "GAJI"){
            redirect('Login');
            }
	}
	
	public function index(){
		$data['pegawai'] = $this->ModelAdmin->view();
		$this->load->view('komponen/header');
		$this->load->view('komponen/sidebar');
		$this->load->view('Slip/TampilPegawai', $data);
		$this->load->view('komponen/footer');
	}

	public function getDataBulan($bulan_tahun){
		$data['total_potongan'] = $this->ModelAdmin->sumPotongan($bulan_tahun);
		$data['total_gaji_bersih'] = $this->ModelAdmin->sumGajiDiterima($bulan_tahun);
		$data['potongan'] = $this->ModelAdmin->getPotonganBulan($bulan_tahun);
		$data['bulan_tahun'] = $bulan_tahun;
		$this->load->view('komponen/header');
		$this->load->view('komponen/sidebar');
		$this->load->view('Slip/TampilPotonganBulan', $data);
		$this->load->view('komponen/footer');
	}

	public function editPegawai(){
		$nip = $this->input->post('nip'); 
		$data = array(
			'nama_pegawai' => $this->input->post('nama_pegawai'), 
			'golongan' => $this->input->post('golongan'), 
			'rekening' => $this->input->post('rekening'), 
			'nama_rekening' => $this->input->post('nama_rekening'), 
			'nama_bank' => $this->input->post('nama_bank'), 
			'email' => $this->input->post('email'), 
		);
		$this->ModelAdmin->editPegawai($data,$nip);
		redirect('admin');
	}

	public function addPegawai(){
		$data = array(
			'nip' => $this->input->post('nip'),
			'nama_pegawai' => $this->input->post('nama_pegawai'), 
			'golongan' => $this->input->post('golongan'), 
			'rekening' => $this->input->post('rekening'), 
			'nama_rekening' => $this->input->post('nama_rekening'), 
			'nama_bank' => $this->input->post('nama_bank'), 
			'email' => $this->input->post('email'), 
		);
		$this->ModelAdmin->addPegawai($data);
		redirect('admin');
	}

	public function potongan(){
		$data['potongan'] = $this->ModelAdmin->getPotongan();
		$this->load->view('komponen/header');
		$this->load->view('komponen/sidebar');
		$this->load->view('Slip/TampilPotongan', $data);
		$this->load->view('komponen/footer');
	}

	 public function getDetailPegawai($nip){
        $data['pegawai'] = $this->ModelAdmin->getDetailPegawai($nip);
        $data['pegawai2'] = $this->ModelAdmin->getNamaPegawai($nip);

        $this->load->view('komponen/header');
        $this->load->view('komponen/sidebar');
        $this->load->view('Slip/TampilanDetailPegawai', $data);
        $this->load->view('komponen/footer');
    }
	
	public function formPegawai(){
		$data = array();	
		if(isset($_POST['preview'])){
			$upload = $this->ModelAdmin->upload_file($this->filename);
			
			if($upload['result'] == "success"){
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx');
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

				$data['sheet'] = $sheet; 
			}else{
				$data['upload_error'] = $upload['error'];
			}
		}
		
		$this->load->view('komponen/header');
		$this->load->view('komponen/sidebar');
		$this->load->view('Slip/ImportPegawai', $data);
		$this->load->view('komponen/footer');
	}

	public function formPotongan(){
		$data = array();	
		if(isset($_POST['preview'])){
			$upload = $this->ModelAdmin->upload_file_gaji($this->filename);
			
			if($upload['result'] == "success"){
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx');
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				$data['sheet'] = $sheet; 
			}else{
				$data['upload_error'] = $upload['error'];
			}
		}
		
		$this->load->view('komponen/header');
		$this->load->view('komponen/sidebar');
		$this->load->view('Slip/ImportPotongan', $data);
		$this->load->view('komponen/footer');
	}
	
	public function import(){
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx');
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		$data = array();
		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){

				array_push($data, array(
		          'nip'=>$row['C'],
		          'nama_pegawai'=>$row['D'],
		          'golongan'=>$row['E'],
		          'rekening'=>$row['F'], 
		          'nama_rekening'=>$row['G'], 
		          'nama_bank'=>$row['H'], 
		          'email'=>$row['AE'], 
        		));

			}
			$numrow++;
		}

		$this->ModelAdmin->insert_multiple($data);
		redirect("admin");
	}

	public function import2(){
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx');
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		$data = array();
		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){

				array_push($data, array(
		          'id_pegawai'=>$row['C'],
		          'bulan_tahun'=>$row['AF'], 
		          'uang_makan'=>$row['J'],
		          'tunker'=>$row['K'], 
		          'p1'=>$row['L'], 
		          'p2'=>$row['M'], 
		          'p3'=>$row['N'], 
		          'p4'=>$row['O'], 
		          'p5'=>$row['P'], 
		          'p6'=>$row['Q'], 
		          'p7'=>$row['R'], 
		          'p8'=>$row['S'], 
		          'p9'=>$row['T'], 
		          'p10'=>$row['U'], 
		          'p11'=>$row['W'], 
		          'p12'=>$row['X'], 
		          'p13'=>$row['Y'], 
		          'p14'=>$row['Z'], 
		          'p15'=>$row['AA'], 
		          'pengurang_tunker'=>$row['V'], 
		          'total_potongan'=>$row['AB'], 
		          'bayar'=>$row['AD'],
        		));

			}
			$numrow++;
		}

		$this->ModelAdmin->insert_multiple_potongan($data);
		redirect("admin/potongan");
	}

	public function deleteDetailPegawai($nip){
		$this->db->where('nip', $nip);
   		$this->db->delete('tb_pegawai');
		redirect('admin');
	}

	public function deletePegawai(){
		$this->db->empty_table('tb_pegawai');
		$this->ModelAdmin->autoIncrement();
		redirect('admin');
	}

	public function deleteAllPotongan(){
		$this->db->empty_table('tb_potongan');
		$this->ModelAdmin->autoIncrementPotongan();
		redirect('admin/potongan');
	}

	public function deletePotongan(){
		$bulan_tahun = $this->input->post('bulan_tahun');
		$this->ModelAdmin->deletePotongan($bulan_tahun);
		redirect('admin/potongan');
	}

	public function send_email_all(){
		$ArrData = $this->input->post('check_list');
		// print_r($ArrData); die;
		$jumlahData = count($ArrData);

		$config = [
               'mailtype'  => 'html',
               'charset'   => 'utf-8',
               'protocol'  => 'smtp',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_user' => 'slipgaji.lpngs@gmail.com',
               'smtp_pass' => 'lpngunungsindur',
               'smtp_port' => 465,
               'crlf'      => "\r\n",
               'newline'   => "\r\n"
           ];
			
		for ($i=0; $i < $jumlahData ; $i++) { 
			$id123 = $ArrData[$i];
            
            $this->db->join('tb_pegawai','tb_potongan.id_pegawai = tb_pegawai.nip');
      	 	$this->db->select('tb_pegawai.id as id, tb_pegawai.nip as nip, tb_pegawai.email as email, tb_pegawai.rekening as rekening, tb_pegawai.nama_pegawai as nama_pegawai, tb_pegawai.golongan as golongan, tb_potongan.bulan_tahun as bulan_tahun, tb_potongan.uang_makan as uang_makan, tb_potongan.tunker as tunker, tb_potongan.p1 as p1, tb_potongan.p2 as p2, tb_potongan.p3 as p3, tb_potongan.p4 as p4, tb_potongan.p5 as p5, tb_potongan.p6 as p6, tb_potongan.p7 as p7, tb_potongan.p8 as p8, tb_potongan.p9 as p9, tb_potongan.p10 as p10, tb_potongan.pengurang_tunker as pengurang_tunker, tb_potongan.total_potongan as total_potongan, tb_potongan.bayar as bayar');

        $data = $this->db->get_where('tb_potongan', array('tb_potongan.id' => $id123))->row();

	           $a1 = 'SLIP GAJI PEGAWAI LAPAS NARKOTIKA KELAS IIA GUNUNG SINDUR BULAN '.$data->bulan_tahun;
	           $a2 = '
	           		<b>SLIP GAJI PEGAWAI <br>
	           		LAPAS NARKOTIKA KELAS IIA GUNUNG SINDUR</b><br><br>

	           		NAMA : '.$data->nama_pegawai.'<br> 
	           		GOLONGAN : '.$data->golongan.'<br>
	           		BULAN : '.$data->bulan_tahun.'<br><br>

	           		<b>PENGHASILAN KOTOR</b><br>
	           		GAJI : Rp.'.number_format($data->bayar,0,',','.').'<br> 
	           		TUNKER : Rp.'.number_format($data->tunker,0,',','.').'<br>
	           		UANG MAKAN : Rp.'.number_format($data->uang_makan,0,',','.').'<br><br>

	           		<b>POTONGAN</b> <br>
	           		<u>KOPERASI</u> <br>
	           		SIMPANAN POKOK : Rp.'.number_format($data->p1,0,',','.').'<br>
	           		SIMPANAN WAJIB : Rp.'.number_format($data->p2,0,',','.').'<br>
	           		PINJAMAN : Rp.'.number_format($data->p3,0,',','.').'<br>
	           		BON KOPERASI : Rp.'.number_format($data->p4,0,',','.').'<br>
	           		WARUNG KOPERASI : Rp.'.number_format($data->p5,0,',','.').'<br>
	           		<u>DARMA WANITA</u> <br>
	           		ARISAN : Rp.'.number_format($data->p6,0,',','.').'<br>
	           		KONSUMSI : Rp.'.number_format($data->p7,0,',','.').'<br>
	           		KAS D.W	: Rp.'.number_format($data->p8,0,',','.').'<br>
	           		DANA SOSIAL	: Rp.'.number_format($data->p9,0,',','.').'<br>
	           		<u>BANK</u> <br>
	           		BRI PAJAJARAN : Rp.'.number_format($data->p10,0,',','.').'<br><br>

	           		<b>FAKTOR PENGURANG</b> <br>
	           		TUNKER : Rp.'.number_format($data->pengurang_tunker,0,',','.').'<br>
	           		TOTAL POTONGAN : Rp.'.number_format($data->total_potongan,0,',','.').'<br><br>

	           		<b>TOTAL GAJI BERSIH : Rp.'.number_format($data->bayar,0,',','.').'</b><br><br>

	           		Slip Gaji Ini Berlaku Hanya Sebagai Pemberitahuan. <br> 
	           		Bukan untuk ditunjukan kepada Instansi Lain (Bank, BPJS atau Instansi Lainnya)<br><br>
	           		Tertanda,<br>
	           		Pembuat Daftar Gaji.
	           		</tr>
	           		</table>';	

	           		// print_r($a2); die;

	        $this->load->library('email', $config);

	        $this->email->from('slipgaji.lpngs@gmail.com', 'LAPAS NARKOTIKA KELAS IIA GUNUNG SINDUR');

	        $this->email->to($data->email);

	        $this->email->subject($a1);

	        $this->email->message($a2);

	        if ($this->email->send()) {
	            echo "<script>alert('Email Berhasil Dikirim!');history.go(-1);</script>";
	        } else {
	            echo "<script>alert('Email Gagal Dikirim!');history.go(-1);</script>";
	        }
		}
    }
}
