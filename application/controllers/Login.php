<?php
Class Login extends CI_Controller{
    private $filename = "import_data";
    
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('ModelLogin');
        $this->load->model('ModelAdmin');
    }

    function index(){
        $this->load->view('login');
    }

    function cekLogin(){
        $data = array('username' => $this->input->post('username'),
                        'password' => $this->input->post('password')
            );
        $hasil = $this->ModelLogin->cekLogin($data);
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['id'] = $sess->id;
                $sess_data['username'] = $sess->username;
                $sess_data['level'] = $sess->level;
                $sess_data['status'] = 'LOGIN';
                $this->session->set_userdata($sess_data);
            }
             if ($this->session->userdata('level')=='GAJI') {
                redirect('admin');
            }
            elseif ($this->session->userdata('level')=='TUNJANGAN') {
                redirect('AdminTunjangan');
            }   
        }else{
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
    }
        

    function logout(){
        $this->session->sess_destroy();
        redirect('Login');
    }

}