<?php
class ModelLogin extends ci_model{
	
	public function __construct(){
		$this->load->database();
    	$this->load->helper('url');
	}

    function cekLogin($data){      
        $cek = $this->db->get_where('tb_user',$data);
        return $cek;
    }
}