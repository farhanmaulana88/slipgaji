<?php
class ModelAdmin extends CI_Model {

	public function __construct(){
		$this->load->database();
    	$this->load->helper('url');
	}

	public function view(){
		$tabel = $this->db->get('tb_pegawai');
		return $tabel;
	}

	public function getID(){
        $this->db->select('id_potongan');
        $data = $this->db->get('tb_pinjaman')->result();
        return $data;
	}

	public function getNIP(){
        $this->db->select('nip');
        $data = $this->db->get('tb_pinjaman')->result();
        return $data;
	}

	public function getDetailPegawai($nip){
        $this->db->select('tb_pegawai.nip, tb_pegawai.rekening, tb_pegawai.email, tb_pegawai.id, tb_pegawai.nama_pegawai, tb_potongan.id as id123, tb_potongan.*');

            $this->db->join('tb_pegawai','tb_potongan.id_pegawai = tb_pegawai.nip');

        $data = $this->db->get_where('tb_potongan', array('tb_pegawai.nip' => $nip));
        return $data->result();
	}

	public function getNamaPegawai($nip){
		$this->db->select('nama_pegawai, nip');
		$data = $this->db->get_where('tb_pegawai', array('nip' => $nip));
        return $data->result();
	}

	function editPegawai($data,$id){
    return  $this->db->update('tb_pegawai',$data,array('nip'=>$id));  
  	} 

  	function addPegawai($data){
    return  $this->db->insert('tb_pegawai',$data);  
  	} 

  	function autoIncrement(){
  		return $this->db->query("ALTER TABLE tb_pegawai AUTO_INCREMENT =1;");
  	}

  	function autoIncrementPotongan(){
  		return $this->db->query("ALTER TABLE tb_potongan AUTO_INCREMENT =1;");
  	}

	function getPotongan()
    {
        $this->db->select('tb_pegawai.nama_pegawai, tb_potongan.*');

            $this->db->join('tb_pegawai','tb_potongan.id_pegawai = tb_pegawai.nip');

        $data = $this->db->get('tb_potongan');
        return $data->result();
    }

    public function sumPotongan($bulan_tahun)
    {
        $this->db->select_sum('total_potongan');
        $this->db->from('tb_potongan');
        $this->db->where('bulan_tahun', $bulan_tahun);
        return $this->db->get()->result();
    }

    public function sumGajiDiterima($bulan_tahun)
    {
        $this->db->select_sum('bayar');
        $this->db->from('tb_potongan');
        $this->db->where('bulan_tahun', $bulan_tahun);
        return $this->db->get()->result();
    }

    function getPotonganBulan($bulan_tahun)
    {
        $this->db->select('tb_pegawai.id, tb_pegawai.nip, tb_pegawai.email, tb_pegawai.rekening, tb_pegawai.nama_pegawai, tb_potongan.*, tb_potongan.id as id123');

            $this->db->join('tb_pegawai','tb_potongan.id_pegawai = tb_pegawai.nip');

        $data = $this->db->get_where('tb_potongan', array('tb_potongan.bulan_tahun' => $bulan_tahun));
        return $data->result();
    }
	
	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload
		
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
	
		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	public function upload_file_gaji($filename){
		$this->load->library('upload'); // Load librari upload
		
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
	
		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	public function insert_multiple($data){
		$this->db->insert_batch('tb_pegawai', $data);
	}

	public function insert_multiple_potongan($data){
		$this->db->insert_batch('tb_potongan', $data);
	}

	function deletePotongan($bulan_tahun){
		$this->db->where('bulan_tahun', $bulan_tahun);
		$this->db->delete('tb_potongan');
	}

}
